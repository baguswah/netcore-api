﻿using System.ComponentModel.DataAnnotations;

namespace Latihan_Net_2.Models
{
    public class LoginModel
    {
        [Required]
        public string? Email { get; set; }
        [Required]
        public string? Password { get; set; }
    }
}

