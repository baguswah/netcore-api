﻿using System.ComponentModel.DataAnnotations;
namespace Latihan_Net_2.Models
{
    public class ModelCompany
    {
        
        [Required]
        public string? Name { get; set; }

        public string? Address { get; set; }

        public string? Telephone { get; set; }

        [Required]

        public DateTime Date { get; set; } = DateTime.Now;
    }
}
