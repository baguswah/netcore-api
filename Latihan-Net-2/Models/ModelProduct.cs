﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
namespace Latihan_Net_2.Models

{
    public class ModelProduct
    {
        [Required]
        public int? IDCompany { get; set; }

        [Required]
        public int? IDBrand { get; set; }
        [Required]
        public string? Name { get; set; }
        [Required]
        public string? Variant { get; set; }
        [Required]
        public string? Price { get; set; }
        [Required]
        public int? IDUser { get; set; }
        [Required]
        public DateTime Date { get; set; } = DateTime.Now;

    }
}
