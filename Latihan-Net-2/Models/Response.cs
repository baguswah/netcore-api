﻿namespace Latihan_Net_2.Models
{
    public class Response<T>
    {
        public T Data { get; set; }
        public string Message { get; set; }
        public int Code { get; set; }
    }
}
