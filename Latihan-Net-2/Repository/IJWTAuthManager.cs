﻿using Dapper;
using Latihan_Net_2.Models;

namespace Latihan_Net_2.Repository
{
    public interface IJWTAuthManager
    {
        Response<string> GenerateJWT(ModelUser user);
        Response<T> Execute_Command<T>(string query, DynamicParameters sp_params);
        Response<List<T>> getUserList<T>();
        Response<List<T>> getCompanyList<T>();
        Response<List<T>> getBrandList<T>();
        Response<List<T>> getProductList<T>();
    }
}
